﻿'use strict'

$(document).ready(function () {
    $('.datepicker').datepicker({ 'dateFormat': 'dd/mm/yy' });
    $('input[name=duurradio]').click(function () {
        var waarde = $('input[name=duurradio]:checked').val();
        switch (waarde) {
            case "1":
                SetVoormiddag();
                break;
            case "2":
                SetNamiddag();
                break;
            case "3":
                SetGanseDag();
                break;
            default:
                break;
        }
    });
});

function Ticket_Change() {
    var ticket = $('#Wizard1_TicketDropDown option:selected');
    var link = "https://itcbe.sharepoint.com/Lists/Ticket/Dispform.aspx?ID=" + ticket.val();
    $('#Wizard1_OmschrijvingTextBox').val(link);
    $('#Wizard1_OnderwerpTextBox').val(ticket.text());
    return true;
}

function SetVoormiddag() {
    $('#Wizard1_startUurDropDown').val("08");
    $('#Wizard1_eindUurDropDown').val("12");
}

function SetNamiddag() {
    $('#Wizard1_startUurDropDown').val("13");
    $('#Wizard1_eindUurDropDown').val("17");
}

function SetGanseDag() {
    $('#Wizard1_startUurDropDown').val("08");
    $('#Wizard1_eindUurDropDown').val("17");
}

function ITCButton_Click() {
    $('#Wizard1_LocatieTextBox').val("Rechterstraat 114, 3500 HASSELT");
    return false;
}

function moveRight_Click() {
    var selected = $('#Wizard1_TechniekerDropDown option:selected');
    var clone = selected.clone();
    $('#Wizard1_SelectedListBox').append(clone);
    //SortSelect($('#SelectedListBox option'));
    return false;
}

function moveLeft_Click() {
    var selected = $('#Wizard1_SelectedListBox option:selected');
    $("#Wizard1_SelectedListBox option[value='" + selected.val() + "']").remove();
    //SortSelect($('#TechniekerDropDown option'));
    return false;
}

function SortSelect(select) {
    select.sort(function (a, b) {
        //return $(a).text > $(b).text;
        if (a.text > b.text) {
            return 1;
        }
        else if (a.text < b.text) {
            return -1;
        }
        else {
            return 0
        }
    });
    $("#Wizard1_TechniekerDropDown").empty().append(select);
}

function ValidateAppointment() {
    var valid = true;
    var errortext = "";
    var strSelected = "";
    var selected = $('#Wizard1_SelectedListBox option');
    selected.each(function (i) {
        var option = $(this);
        strSelected += option.val() + ",";
    });
    strSelected = strSelected.substring(0, strSelected.length - 1);
    $("#Wizard1_selectedHiddenField").val(strSelected);
    var onderwerp = $('#Wizard1_OnderwerpTextBox').val();
    var pas = $('#Wizard1_passwordTextBox').val();

    if (strSelected === "") {
        valid = false;
        errortext += "Geen technieker(s) gekozen!\n";
    }

    if (onderwerp === "") {
        valid = false;
        errortext += "Geen onderwerp!\n";
    }

    if (pas === ""){
        valid = false;
        errortext += "Geen paswoord!";
    }

    if (!valid) {
        alert(errortext);
    }

    return valid;
}
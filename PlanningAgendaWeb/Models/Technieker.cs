﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlanningAgendaWeb.Models
{
    public class Technieker
    {
        public string Email { get; set; }
        public int ID { get; set; }
        public String Naam { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlanningAgendaWeb.Models
{
    public class Klant
    {
        public int ID { get; set; }
        public string Naam { get; set; }
        public string Adres { get; set; }
        public string Postcode { get; set; }
        public string Locatie { get; set; }
        public string Telefoon { get; set; }
        public string Fax { get; set; }
    }
}
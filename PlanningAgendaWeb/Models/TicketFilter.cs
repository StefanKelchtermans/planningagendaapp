﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlanningAgendaWeb.Models
{
    public class TicketFilter
    {
        private DateTime? _opvolgdatum;
        public string Klantid { get; set; }
        public string Groep { get; set; }
        public DateTime? Opvolgdatum
        {
            get
            {
                return _opvolgdatum;
            }
            set
            {
                _opvolgdatum = value;
            }
        }
        public string Technieker { get; set; }

        public TicketFilter()
        {

        }
        public TicketFilter(string klantid, string groep, DateTime? opvolgdatum, string technieker)
        {
            Klantid = klantid;
            Groep = groep;
            Opvolgdatum = opvolgdatum;
            Technieker = technieker;  
        }

        public string GetSharePointDate()
        {
            if (_opvolgdatum != null)
            {
                DateTime date = (DateTime)_opvolgdatum;
                return date.Year + "-" + date.Month + "-" + date.Day + "T01:00:00";
            }
            else
                return "";
        }
    }
}
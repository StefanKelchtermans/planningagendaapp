﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlanningAgendaWeb.Models
{
    public class Ticket
    {
        public int ID { get; set; }
        public string Omschrijving { get; set; }
        public string KlantAdres { get; set; }
    }
}
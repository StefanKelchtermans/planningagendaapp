﻿using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace PlanningAgendaWeb.Models
{
    public class SharePointConnection
    {
        //private ClientContext ctx;
        //private const string TargetSite = "https://itcbe.sharepoint.com/";

        public SharePointConnection()
        {
            //try
            //{
            //    ctx = new ClientContext(TargetSite);
            //    ctx.AuthenticationMode = ClientAuthenticationMode.FormsAuthentication;
            //    FormsAuthenticationLoginInfo logininfo = new FormsAuthenticationLoginInfo { LoginName = "stefan.kelchtermans@itc.be", Password = "Left4dead" };
            //    ctx.FormsAuthenticationLoginInfo = logininfo;
            //    Web web = ctx.Web;
            //    ctx.Load(web);
            //    ctx.ExecuteQuery();

            //}
            //catch (Exception ex)
            //{

            //}
        }
        public List<Klant> GetKlanten(string hostweb, string contextToken, HttpRequest request)
        {
            try
            {
                List<Klant> klanten = new List<Klant>();
                using (var ctx = TokenHelper.GetClientContextWithContextToken(hostweb, contextToken, request.Url.Authority))
                {
                    List myList = ctx.Web.Lists.GetByTitle("Klantenlijst");
                    //int id = p.LookupId;

                    StringBuilder caml = new StringBuilder();
                    caml.Append(@"<View><Query><OrderBy><FieldRef Name = 'Naam1'/></OrderBy></Query><ViewFields><FieldRef Name = 'ID'/><FieldRef Name = 'Naam1'/></ViewFields></View >"); 

                    CamlQuery query = new CamlQuery();
                    query.ViewXml = caml.ToString();

                    ListItemCollection myItems = myList.GetItems(query);
                    ctx.Load(myItems);
                    ctx.ExecuteQuery();
                    
                    foreach (var item in myItems)
                    {
                        Klant klant = new Klant();
                        klant.ID = Convert.ToInt32(item["ID"]);
                        if (item["Naam1"] != null)
                            klant.Naam = item["Naam1"].ToString();
                        //if (item["Straat_x0020__x002b__x0020_numme"] != null)
                        //    klant.StraatEnNummer = item["Straat_x0020__x002b__x0020_numme"].ToString();
                        //if (item["Postcode"] != null)
                        //    klant.Postcode = item["Postcode"].ToString();
                        //if (item["Locatie"] != null)
                        //    klant.Locatie = item["Locatie"].ToString();
                        //if (item["Telefoon"] != null)
                        //    klant.Telefoon = item["Telefoon"].ToString();
                        //if (item["Fax"] != null)
                        //    klant.Fax = item["Fax"].ToString();
                        //if (item["Vertegenwoordiger"] != null)
                        //    klant.Vertegenwoordiger = item["Vertegenwoordiger"].ToString();
                        //if (item["Sector"] != null)
                        //    klant.Sector = item["Sector"].ToString();
                        //if (item["Managed_x0020_Services"] != null)
                        //    klant.ManagedServices = item["Managed_x0020_Services"].ToString();
                        klanten.Add(klant);
                    }
                    return klanten;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal List<string> GetGroepen(string hostweb, string contextToken, HttpRequest request)
        {
            try
            {
                List<string> groepen = new List<string>();
                using (var ctx = TokenHelper.GetClientContextWithContextToken(hostweb, contextToken, request.Url.Authority))
                {
                    List myList = ctx.Web.Lists.GetByTitle("Klantenlijst");
                    ctx.Load(myList);
                    //ctx.ExecuteQuery();
                    var groep = ctx.CastTo<FieldChoice>(myList.Fields.GetByInternalNameOrTitle("Groep"));
                    ctx.Load(groep, g => g.Choices);
                    ctx.ExecuteQuery();
                    foreach (string item in groep.Choices)
                    {
                        groepen.Add(item.ToString());
                    }
                    return groepen;
                }
            }
             catch (Exception ex)
            {
                return null;
            }
        }

        public Klant GetKlantByID(string klantId, string hostweb, string contextToken, HttpRequest request)
        {
            try
            {
                Klant klant = new Klant();
                using (var ctx = TokenHelper.GetClientContextWithContextToken(hostweb, contextToken, request.Url.Authority))
                {
                    List myList = ctx.Web.Lists.GetByTitle("Klantenlijst");
                    //int id = p.LookupId;

                    StringBuilder caml = new StringBuilder();
                    caml.Append("<View><Query>");
                    caml.Append("<Where>");
                    caml.Append("<Eq><FieldRef Name='ID' /><Value Type='Counter'>" + klantId + "</Value></Eq>");
                    caml.Append("</Where>");
                    caml.Append("</Query>");
                    caml.Append("<ViewFields>");
                    caml.Append("<FieldRef Name='ID'/>");
                    caml.Append("<FieldRef Name='Naam1'/>");
                    caml.Append("<FieldRef Name='Straat_x0020__x002b__x0020_numme'/>");
                    caml.Append("<FieldRef Name='Locatie'/>");
                    caml.Append("<FieldRef Name='Postcode'/>");
                    caml.Append("<FieldRef Name='Telefoon'/>");
                    caml.Append("<FieldRef Name='Fax'/>");
                    caml.Append("</ViewFields>");
                    caml.Append("<RowLimit>1</RowLimit>");
                    caml.Append("</View>");


                    CamlQuery query = new CamlQuery();
                    query.ViewXml = caml.ToString();

                    ListItemCollection myItems = myList.GetItems(query);
                    ctx.Load(myItems);
                    ctx.ExecuteQuery();

                    foreach (var item in myItems)
                    {
                        klant.ID = Convert.ToInt32(item["ID"]);
                        if (item["Naam1"] != null)
                            klant.Naam = item["Naam1"].ToString();
                        if (item["Straat_x0020__x002b__x0020_numme"] != null)
                            klant.Adres = item["Straat_x0020__x002b__x0020_numme"].ToString();
                        if (item["Postcode"] != null)
                            klant.Postcode = item["Postcode"].ToString();
                        if (item["Locatie"] != null)
                            klant.Locatie = item["Locatie"].ToString();
                        if (item["Telefoon"] != null)
                            klant.Telefoon = item["Telefoon"].ToString();
                        if (item["Fax"] != null)
                            klant.Fax = item["Fax"].ToString();
                    }
                    return klant;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<Ticket> GetTickets(string klantId, string hostweb, string contextToken, HttpRequest request, int drempelwaarde)
        {
            try
            {
                List<Ticket> tickets = new List<Ticket>();
                using (var ctx = TokenHelper.GetClientContextWithContextToken(hostweb, contextToken, request.Url.Authority))
                {
                    List myList = ctx.Web.Lists.GetByTitle("Ticket");
                    //int id = p.LookupId;

                    StringBuilder caml = new StringBuilder();
                    caml.Append("<View>");
                    caml.Append("<Query>");
                    caml.Append("<Where><And><And>");
                    caml.Append("<Eq><FieldRef Name='Klant' LookupId='True'/><Value Type='Lookup'>" + klantId + "</Value></Eq>");
                    caml.Append("<Gt><FieldRef Name='ID' /><Value Type='Counter'>" + drempelwaarde + "</Value></Gt>");
                    caml.Append("</And>");
                    caml.Append("<Neq><FieldRef Name='Status'/><Value Type='Choice'>Afgesloten</Value></Neq>");
                    caml.Append("</And>");
                    caml.Append("</Where>");
                    caml.Append("<OrderBy><FieldRef Name='ID' Ascending ='TRUE'/></OrderBy>");
                    caml.Append("</Query>");
                    caml.Append("<ViewFields><FieldRef Name='ID'/><FieldRef Name='Title'/><FieldRef Name='Klant'/></ViewFields>");
                    caml.Append("</View>");
                    CamlQuery query = new CamlQuery();
                    query.ViewXml = caml.ToString();

                    ListItemCollection myItems = myList.GetItems(query);
                    ctx.Load(myItems);
                    ctx.ExecuteQuery();

                    foreach (var item in myItems)
                    {
                        Ticket ticket = new Ticket();
                        ticket.ID = Convert.ToInt32(item["ID"]);
                        if (item["Title"] != null)
                            ticket.Omschrijving = item["Title"].ToString();
                        tickets.Add(ticket);
                    }
                    return tickets;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal Ticket GetTicketByID(string ticketid, string hostweb, string contextToken, HttpRequest request)
        {
            try
            {
                Ticket ticket = new Ticket();
                using (var ctx = TokenHelper.GetClientContextWithContextToken(hostweb, contextToken, request.Url.Authority))
                {
                    List myList = ctx.Web.Lists.GetByTitle("Ticket");
                    //int id = p.LookupId;

                    StringBuilder caml = new StringBuilder();
                    caml.Append("<View>");
                    caml.Append("<Query>");
                    caml.Append("<Where>");
                    caml.Append("<Eq><FieldRef Name='ID' /><Value Type='Counter'>" + ticketid + "</Value></Eq>");
                    caml.Append("</Where>");
                    caml.Append("</Query>");
                    caml.Append("<ViewFields>");
                    caml.Append("<FieldRef Name='Title'/>");
                    caml.Append("<FieldRef Name='Klant_x003a_Straat_x0020__x002b_'/>");
                    caml.Append("<FieldRef Name='Klant_x003a_Postcode'/>");
                    caml.Append("<FieldRef Name='Klant_x003a_Locatie'/>");
                    caml.Append("<FieldRef Name='ID'/>");
                    caml.Append("</ViewFields>");
                    caml.Append("</View>");



                    CamlQuery query = new CamlQuery();
                    query.ViewXml = caml.ToString();

                    ListItemCollection myItems = myList.GetItems(query);
                    ctx.Load(myItems);
                    ctx.ExecuteQuery();

                    foreach (var item in myItems)
                    {
                        ticket.ID = Convert.ToInt32(item["ID"]);
                        string omschrijving = item["Title"] == null ? "" : item["Title"].ToString();
                        string adres = "";
                        if (item["Klant_x003a_Straat_x0020__x002b_"] != null)
                            adres = ((FieldLookupValue)item["Klant_x003a_Straat_x0020__x002b_"]).LookupValue;
                        string postcode = "";
                        if(item["Klant_x003a_Postcode"] != null)
                            postcode = ((FieldLookupValue)item["Klant_x003a_Postcode"]).LookupValue;
                        string plaats = "";
                        if(item["Klant_x003a_Locatie"] != null)
                            plaats = ((FieldLookupValue)item["Klant_x003a_Locatie"]).LookupValue;
                        ticket.Omschrijving = omschrijving;
                        ticket.KlantAdres = adres + " " + postcode + ": " + plaats.ToUpper();
                    }
                    return ticket;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        internal List<Ticket> GetTicketsByFilter(TicketFilter filter, string hostweb, string contextToken, HttpRequest request, int drempelwaarde)
        {
            try
            {
                List<Ticket> tickets = new List<Ticket>();
                using (var ctx = TokenHelper.GetClientContextWithContextToken(hostweb, contextToken, request.Url.Authority))
                {
                    List myList = ctx.Web.Lists.GetByTitle("Ticket");
                    //int id = p.LookupId;

                    StringBuilder caml = new StringBuilder();
                    caml.Append("<View>");
                    caml.Append("<Query>");
                    caml.Append("<Where><And>");
                    
                    if (!string.IsNullOrEmpty(filter.Klantid))
                        caml.Append("<And>");

                    if (!string.IsNullOrEmpty(filter.GetSharePointDate()))
                        caml.Append("<And>");

                    if (!string.IsNullOrEmpty(filter.Technieker))
                        caml.Append("<And>");

                    if (!string.IsNullOrEmpty(filter.Groep))
                        caml.Append("<And>");

                    caml.Append("<Neq><FieldRef Name='Status'/><Value Type='Choice'>Afgesloten</Value></Neq>");
                    caml.Append("<Gt><FieldRef Name='ID' /><Value Type='Counter'>" + drempelwaarde + "</Value></Gt>");
                    caml.Append("</And>");

                    if (filter.Klantid != "")
                    {
                        caml.Append("<Eq><FieldRef Name='Klant' LookupId='True'/><Value Type='Lookup'>" + filter.Klantid + "</Value></Eq>");
                        caml.Append("</And>");
                    }

                    if (filter.Opvolgdatum != null)
                    {
                        string datum = filter.GetSharePointDate();
                        caml.Append("<Eq><FieldRef Name='Opvolgingsdatum'/><Value Type='DateTime'>" + datum + "</Value></Eq>");
                        caml.Append("</And>");
                    }

                    if (!string.IsNullOrEmpty(filter.Technieker))
                    {
                        caml.Append("<Eq><FieldRef Name='Toegewezen_x0020_aan' /><Value Type='User'>" + filter.Technieker + "</Value></Eq>");
                        caml.Append("</And>");
                    }

                    if (!string.IsNullOrEmpty(filter.Groep))
                    {
                        caml.Append("<Eq><FieldRef Name='Klant_x003a_Groep_x0020_tekst' /><Value Type='Lookup'>" + filter.Groep + "</Value></Eq>");
                        caml.Append("</And>");
                    }

                    caml.Append("</Where>");
                    caml.Append("<OrderBy><FieldRef Name='ID' Ascending ='TRUE'/></OrderBy>");
                    caml.Append("</Query>");
                    caml.Append("<ViewFields><FieldRef Name='ID'/><FieldRef Name='Title'/><FieldRef Name='Klant'/></ViewFields>");
                    caml.Append("</View>");
                    CamlQuery query = new CamlQuery();
                    query.ViewXml = caml.ToString();

                    ListItemCollection myItems = myList.GetItems(query);
                    ctx.Load(myItems);
                    ctx.ExecuteQuery();

                    foreach (var item in myItems)
                    {
                        Ticket ticket = new Ticket();
                        ticket.ID = Convert.ToInt32(item["ID"]);
                        if (item["Title"] != null)
                            ticket.Omschrijving = item["Title"].ToString();
                        tickets.Add(ticket);
                    }
                    return tickets;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public int GetDrempelwaarde(string hostweb, string contextToken, HttpRequest request)
        {
            int waarde = 0;
            using (var ctx = TokenHelper.GetClientContextWithContextToken(hostweb, contextToken, request.Url.Authority))
            {
                List myList = ctx.Web.Lists.GetByTitle("Drempelwaarden");
                //int id = p.LookupId;

                StringBuilder caml = new StringBuilder();
                caml.Append("<View>");
                caml.Append("<Query>");
                caml.Append("</Query>");
                caml.Append("<ViewFields><FieldRef Name='ID'/><FieldRef Name='Title'/><FieldRef Name='drempelwaarde'/></ViewFields>");
                caml.Append("</View>");
                CamlQuery query = new CamlQuery();
                query.ViewXml = caml.ToString();

                ListItemCollection myItems = myList.GetItems(query);
                ctx.Load(myItems);
                ctx.ExecuteQuery();

                foreach (var item in myItems)
                {
                    string lijst = item["Title"].ToString();
                    if (lijst == "Ticket")
                        waarde = Convert.ToInt32(item["drempelwaarde"]);
                }

                return waarde;
            }
        }

        public List<Technieker> GetTechniekers(string hostweb, string contextToken, HttpRequest request)
        {
            List<Technieker> techniekers = new List<Technieker>();
            using (var ctx = TokenHelper.GetClientContextWithContextToken(hostweb, contextToken, request.Url.Authority))
            {
                GroupCollection collGroup = ctx.Web.SiteGroups;
                Group group = ctx.Web.SiteGroups.GetByName("Medewerkers");
                UserCollection collUser = group.Users;

                ctx.Load(collUser, users => users.Include(user => user.Title, user => user.LoginName, user => user.Email).OrderBy(user => user.LoginName));
                ctx.ExecuteQuery();
                foreach (User oUser in collUser)
                {
                    Technieker tech = new Technieker();
                    tech.Naam = oUser.Title;
                    tech.Email = oUser.Email;
                    techniekers.Add(tech);
                }

                return techniekers;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlanningAgendaWeb.Models
{
    public class Afspraak
    {
        public string UserEmail { get; set; }
        public string Omschrijving { get; set; }
        public string Paswoord { get; set; }
        public string Startuur { get; set; }
        public string Einduur { get; set; }
        public string Onderwerp { get; set; }
        public string Locatie { get; set; }
        public string Categorie { get; set; }
        public List<string> Geadresseerden { get; set; }
    }
}
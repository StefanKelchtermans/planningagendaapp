﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PlanningAgendaWeb.Models
{
    public static class DateConversions
    {
        public static DateTime GetDate(string date)
        {
            var datearray = date.Split('/');
            int year = int.Parse(datearray[2]);
            int month = int.Parse(datearray[1]);
            int day = int.Parse(datearray[0]);
            DateTime result = new DateTime(year, month, day);

            return result;
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Async="true" Inherits="PlanningAgendaWeb.Default" EnableEventValidation="false"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Agenda planning</title>
    <link href="../CSS/MainStyle.css" rel="stylesheet" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="../Scripts/App.js"></script>
    <link href="../Content/bootstrap-theme.min.css" rel="stylesheet" />
    <link href="../Content/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="jumbotron bglightgray">
            <div class="container">
                <h1>
                    <img src="../Images/ITC_logo.gif" class="img-rounded" style="width: 60px; height: 60px;" />
                    ITC belgium
                </h1>
                <p>Plaats tickets in de Agenda van de Technieker</p>
            </div>
        </div>
        <div class="container" style="background:White;">
            <div class="panel">
                <asp:Label runat="server" ID="message"></asp:Label>
                <asp:Wizard ID="Wizard1" runat="server" ActiveStepIndex="0" Width="900px" OnFinishButtonClick="Wizard1_FinishButtonClick">
                    <FinishNavigationTemplate>
                        <asp:Button ID="FinishPreviousButton" runat="server" CausesValidation="False" class="btn btn-default" CommandName="MovePrevious" Text="Vorige" />
                        <asp:Button ID="FinishButton" runat="server"  OnClientClick="return ValidateAppointment();"  OnClick="SaveButton_Click" class="btn btn-primary" UseSubmitBehavior="true"  CommandName="MoveComplete" Text="Voltooien" />
                    </FinishNavigationTemplate>
                    <SideBarTemplate>
                        <asp:DataList ID="SideBarList" runat="server">
                            <ItemTemplate>
                                <p><asp:LinkButton ID="SideBarButton" runat="server"></asp:LinkButton></p>
                            </ItemTemplate>
                            <SelectedItemStyle Font-Bold="True" />
                        </asp:DataList>
                    </SideBarTemplate>
                    <StartNavigationTemplate>
                        <asp:Button ID="StartNextButton" runat="server" CommandName="MoveNext" class="btn btn-default" OnClick="StartNextButton_Click" Text="Volgende"/>
                    </StartNavigationTemplate>
                    <WizardSteps>
                        <asp:WizardStep runat="server" Title="Filter">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="KlantDropDown" class="col-sm-3 control-label">Klant:</label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList runat="server" CssClass="form-control" ID="KlantDropDown" Width="500" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="GroupDropDown" class="col-sm-3 control-label">Groep:</label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="GroupDropDown" runat="server" CssClass="form-control" Width="500px"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="TechniekerDropDown" class="col-sm-3 control-label">Technieker:</label>
                                    <div class="col-sm-9">
                                        <asp:DropDownList ID="TechniekerKeuzeDropDown" runat="server" CssClass="form-control" Width="500px"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="OpvolgTextBox" class="col-sm-3 control-label">Opvolgingsdatum:</label>
                                    <div class="col-sm-9">
                                        <asp:TextBox ID="OpvolgTextBox" CssClass="form-control datepicker" runat="server" Width="218px"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </asp:WizardStep>
                        <asp:WizardStep runat="server" Title="Formulier">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="TicketDropDown" class="col-sm-2 control-label">Ticket:</label>
                                    <div class="col-sm-10">
                                        <asp:DropDownList runat="server" CssClass="form-control" AutoPostBack="true" ID="TicketDropDown" Width="500" OnSelectedIndexChanged="TicketDropDown_SelectedIndexChanged" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Onderwerp</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="OnderwerpTextBox" runat="server" CssClass="form-control" Width="500" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-sm-2 control-label">Locatie</label>
                                    <div class="col-sm-10 form-inline">
                                        <asp:TextBox ID="LocatieTextBox" runat="server" CssClass="form-control" Width="400" />
                                        <button type="button" class="btn btn-default" style="width: 95px;" onclick="return ITCButton_Click();">ITC</button>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="datumTextBox" class="col-sm-2 control-label">Startuur:</label>
                                    <div class="col-sm-10 form-inline">
                                        <asp:TextBox runat="server" ID="startdatumTextBox" CssClass="form-control datepicker" Width="350" />
                                        <asp:DropDownList runat="server" ID="startUurDropDown" CssClass="form-control" Width="70">
                                            <asp:ListItem Value="00">00</asp:ListItem>
                                            <asp:ListItem Value="01">01</asp:ListItem>
                                            <asp:ListItem Value="02">02</asp:ListItem>
                                            <asp:ListItem Value="03">03</asp:ListItem>
                                            <asp:ListItem Value="04">04</asp:ListItem>
                                            <asp:ListItem Value="05">05</asp:ListItem>
                                            <asp:ListItem Value="06">06</asp:ListItem>
                                            <asp:ListItem Value="07">07</asp:ListItem>
                                            <asp:ListItem Value="08">08</asp:ListItem>
                                            <asp:ListItem Value="09">09</asp:ListItem>
                                            <asp:ListItem Value="10">10</asp:ListItem>
                                            <asp:ListItem Value="11">11</asp:ListItem>
                                            <asp:ListItem Value="12">12</asp:ListItem>
                                            <asp:ListItem Value="13">13</asp:ListItem>
                                            <asp:ListItem Value="14">14</asp:ListItem>
                                            <asp:ListItem Value="15">15</asp:ListItem>
                                            <asp:ListItem Value="16">16</asp:ListItem>
                                            <asp:ListItem Value="17">17</asp:ListItem>
                                            <asp:ListItem Value="18">18</asp:ListItem>
                                            <asp:ListItem Value="19">19</asp:ListItem>
                                            <asp:ListItem Value="20">20</asp:ListItem>
                                            <asp:ListItem Value="21">21</asp:ListItem>
                                            <asp:ListItem Value="22">22</asp:ListItem>
                                            <asp:ListItem Value="23">23</asp:ListItem>
                                            <asp:ListItem Value="00">00</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:DropDownList runat="server" ID="startMinuutDropDown" CssClass="form-control" Width="70">
                                            <asp:ListItem Value=":00:00">00</asp:ListItem>
                                            <asp:ListItem Value=":05:00">05</asp:ListItem>
                                            <asp:ListItem Value=":10:00">10</asp:ListItem>
                                            <asp:ListItem Value=":15:00">15</asp:ListItem>
                                            <asp:ListItem Value=":20:00">20</asp:ListItem>
                                            <asp:ListItem Value=":25:00">25</asp:ListItem>
                                            <asp:ListItem Value=":30:00">30</asp:ListItem>
                                            <asp:ListItem Value=":35:00">35</asp:ListItem>
                                            <asp:ListItem Value=":40:00">40</asp:ListItem>
                                            <asp:ListItem Value=":45:00">45</asp:ListItem>
                                            <asp:ListItem Value=":50:00">50</asp:ListItem>
                                            <asp:ListItem Value=":55:00">55</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="datumTextBox" class="col-sm-2 control-label">Einduur:</label>
                                    <div class="col-sm-10 form-inline">
                                        <asp:TextBox ID="EinddatumTextBox" runat="server" CssClass="form-control datepicker" Width="350" />
                                        <asp:DropDownList runat="server" ID="eindUurDropDown" CssClass="form-control" Width="70">
                                            <asp:ListItem Value="00">00</asp:ListItem>
                                            <asp:ListItem Value="01">01</asp:ListItem>
                                            <asp:ListItem Value="02">02</asp:ListItem>
                                            <asp:ListItem Value="03">03</asp:ListItem>
                                            <asp:ListItem Value="04">04</asp:ListItem>
                                            <asp:ListItem Value="05">05</asp:ListItem>
                                            <asp:ListItem Value="06">06</asp:ListItem>
                                            <asp:ListItem Value="07">07</asp:ListItem>
                                            <asp:ListItem Value="08">08</asp:ListItem>
                                            <asp:ListItem Value="09">09</asp:ListItem>
                                            <asp:ListItem Value="10">10</asp:ListItem>
                                            <asp:ListItem Value="11">11</asp:ListItem>
                                            <asp:ListItem Value="12">12</asp:ListItem>
                                            <asp:ListItem Value="13">13</asp:ListItem>
                                            <asp:ListItem Value="14">14</asp:ListItem>
                                            <asp:ListItem Value="15">15</asp:ListItem>
                                            <asp:ListItem Value="16">16</asp:ListItem>
                                            <asp:ListItem Value="17">17</asp:ListItem>
                                            <asp:ListItem Value="18">18</asp:ListItem>
                                            <asp:ListItem Value="19">19</asp:ListItem>
                                            <asp:ListItem Value="20">20</asp:ListItem>
                                            <asp:ListItem Value="21">21</asp:ListItem>
                                            <asp:ListItem Value="22">22</asp:ListItem>
                                            <asp:ListItem Value="23">23</asp:ListItem>
                                            <asp:ListItem Value="00">00</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:DropDownList runat="server" ID="EindMinuutDropDown" CssClass="form-control" Width="70">
                                            <asp:ListItem Value=":00:00">00</asp:ListItem>
                                            <asp:ListItem Value=":05:00">05</asp:ListItem>
                                            <asp:ListItem Value=":10:00">10</asp:ListItem>
                                            <asp:ListItem Value=":15:00">15</asp:ListItem>
                                            <asp:ListItem Value=":20:00">20</asp:ListItem>
                                            <asp:ListItem Value=":25:00">25</asp:ListItem>
                                            <asp:ListItem Value=":30:00">30</asp:ListItem>
                                            <asp:ListItem Value=":35:00">35</asp:ListItem>
                                            <asp:ListItem Value=":40:00">40</asp:ListItem>
                                            <asp:ListItem Value=":45:00">45</asp:ListItem>
                                            <asp:ListItem Value=":50:00">50</asp:ListItem>
                                            <asp:ListItem Value=":55:00">55</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="categorieDropDown" class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <label class="radio-inline">
                                            <input type="radio" name="duurradio" value="1">Voormiddag</label>
                                        <label class="radio-inline">
                                            <input type="radio" name="duurradio" value="2">Namiddag</label>
                                        <label class="radio-inline">
                                            <input type="radio" name="duurradio" checked="checked" value="3">Ganse dag</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="categorieDropDown" class="col-sm-2 control-label">Categorie:</label>
                                    <div class="col-sm-10">
                                        <asp:DropDownList ID="categorieDropDown" runat="server" CssClass="form-control" Width="500">
                                            <asp:ListItem Value="Standaard">Standaard</asp:ListItem>
                                            <asp:ListItem Value="Helpdesk/Telefoongesprek">Helpdesk/Telefoongesprek</asp:ListItem>
                                            <asp:ListItem Value="Materiaal Meenemen">Materiaal Meenemen</asp:ListItem>
                                            <asp:ListItem Value="Moet Doorgaan">Moet Doorgaan</asp:ListItem>
                                            <asp:ListItem Value="Verlof/Vrij/Ziek">Verlof/Vrij/Ziek</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="OmschrijvingTextBox" class="col-sm-2 control-label">Inhoud:</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="OmschrijvingTextBox" runat="server" CssClass="form-control" Wrap="true" Width="500" Height="100" TextMode="MultiLine" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <asp:HiddenField ID="selectedHiddenField" runat="server" ValidateRequestMode="Disabled" />
                                    <label for="TechniekerDropDown" class="col-sm-2 control-label">Technieker(s):</label>
                                    <div class="col-sm-10">
                                        <div class="floatleft">
                                            <asp:ListBox runat="server" CssClass="form-control" ID="TechniekerDropDown" Rows="6" Width="200"></asp:ListBox>
                                        </div>
                                        <div class="floatleft midsection">
                                            <button type="button" class="btn-default moveButtons" onclick="return moveRight_Click();">&gt;&gt;</button><br />
                                            <button type="button" class="btn-default moveButtons" onclick="return moveLeft_Click();">&lt;&lt;</button>
                                        </div>
                                        <div class="floatleft">
                                            <asp:ListBox ID="SelectedListBox" runat="server" Rows="6" Width="200" CssClass="form-control"></asp:ListBox>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="passwordTextBox" class="col-sm-2 control-label">Paswoord:</label>
                                    <div class="col-sm-10">
                                        <asp:TextBox ID="passwordTextBox" runat="server" CssClass="form-control" Width="500" TextMode="Password" />
                                    </div>
                                </div>
<%--                                <div class="form-group">
                                    <label for="SaveButton" class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10 ">
                                        <asp:Button type="button" runat="server" ID="SaveButton" Text="Opslaan" Width="100" OnClientClick="return ValidateAppointment();" class="btn btn-primary"></asp:Button>
                                    </div>
                                </div>--%>
                            </div>
                        </asp:WizardStep>
                    </WizardSteps>
                </asp:Wizard>
            </div>
        </div>
        <p></p>
        <div class="jumbotron bglightgray">
            <blockquote>
                <p>Webdesign:</p>
                <footer>
                    ITC belgium<br />
                </footer>
                <footer>
                    mail: <a href="mailto:info@itc.be">info@itc.be</a>
                </footer>
                <footer>
                    web: <a href="http://www.itc.be">itc.be</a>
                </footer>
                <footer>
                    tel:+32(0)11 28 63 20
                </footer>
            </blockquote>
        </div>
    </form>
</body>
</html>

﻿using PlanningAgendaWeb.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;

namespace PlanningAgendaWeb
{
    public partial class Default : System.Web.UI.Page
    {

        private static HttpRequest myrequest;
        private static string spapptoken;
        private static string hostweb;
        private static int drempelwaarde;

        protected void Page_PreInit(object sender, EventArgs e)
        {
            Uri redirectUrl;
            switch (SharePointContextProvider.CheckRedirectionStatus(Context, out redirectUrl))
            {
                case RedirectionStatus.Ok:
                    return;
                case RedirectionStatus.ShouldRedirect:
                    Response.Redirect(redirectUrl.AbsoluteUri, endResponse: true);
                    break;
                case RedirectionStatus.CanNotRedirect:
                    Response.Write("An error occurred while processing your request.");
                    Response.End();
                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            myrequest = Page.Request;
            hostweb = Page.Request["SPHostUrl"];
            if (!Page.IsPostBack)
            {
                spapptoken = TokenHelper.GetContextTokenFromRequest(Request);
                SharePointConnection con = new SharePointConnection();
                drempelwaarde = con.GetDrempelwaarde(hostweb, spapptoken, myrequest);
                LoadData();
            }
        }

        private void LoadData()
        {
            string vandaag = DateTime.Today.ToShortDateString();
            startdatumTextBox.Text = vandaag;
            EinddatumTextBox.Text = vandaag;
            startUurDropDown.SelectedIndex = 8;
            eindUurDropDown.SelectedIndex = 17;

            SharePointConnection con = new SharePointConnection();
            List<Technieker> techniekers = con.GetTechniekers(hostweb, spapptoken, myrequest);
            Technieker legeTechnieker = new Technieker();
            legeTechnieker.Email = "";
            legeTechnieker.Naam = "";
            techniekers.Insert(0, legeTechnieker);
            if (techniekers != null)
            {
                foreach (Technieker tech in techniekers)
                {
                    ListItem item = new ListItem();
                    item.Value = tech.Email;
                    item.Text = tech.Naam;
                    TechniekerDropDown.Items.Add(item);
                    ListItem userItem = new ListItem();
                    userItem.Value = tech.Naam;
                    userItem.Text = tech.Naam;
                    TechniekerKeuzeDropDown.Items.Add(userItem);
                    //UserDropDown.Items.Add(userItem);
                }
            }

            List<Klant> klanten = con.GetKlanten(hostweb, spapptoken, myrequest);
            Klant legeklant = new Klant();
            legeklant.ID = 0;
            legeklant.Naam = "";
            klanten.Insert(0, legeklant);
            if (klanten != null)
            {
                foreach (Klant klant in klanten)
                {
                    ListItem item = new ListItem();
                    item.Value = klant.ID.ToString();
                    item.Text = klant.Naam;
                    KlantDropDown.Items.Add(item);
                }
            }

            List<string> groepen = con.GetGroepen(hostweb, spapptoken, myrequest);
            groepen.Insert(0, "");
            if (groepen != null)
            {
                foreach (string groep in groepen)
                {
                    ListItem item = new ListItem();
                    item.Value = groep;
                    item.Text = groep;
                    GroupDropDown.Items.Add(item);
                }
            }
        }

        protected void KlantDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            //DropDownList list = (DropDownList)sender;
            //ListItem klant = list.SelectedItem;
            //Klant selklant = con.GetKlantByID(klant.Value, hostweb, spapptoken, myrequest);
            //if (selklant != null)
            //{
            //    string locatie = selklant.Adres + ", " + selklant.Postcode + " " + selklant.Locatie.ToUpper();
            //    LocatieTextBox.Text = locatie;
            //}
        }

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            Afspraak afspraak = GetAfspraak();
            SaveAfspraak(afspraak);
        }

        private void SaveAfspraak(Afspraak afspraak)
        {
            //try
            //{
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Afspraak));
            MemoryStream mem = new MemoryStream();
            ser.WriteObject(mem, afspraak);
            string data = Encoding.UTF8.GetString(mem.ToArray(), 0, (int)mem.Length);
            WebClient webclient = new WebClient();
            webclient.Headers["Content-type"] = "application/json";
            webclient.Encoding = Encoding.UTF8;
            Uri uri = new Uri(@"http://afspraakservice.itc.be/AfspraakService.svc/SaveAfspraak");
            //webclient.UploadString(@"http://afspraakservice.itc.be/AfspraakService.svc/SaveAfspraak", "POST", data);
            webclient.UploadStringCompleted += new UploadStringCompletedEventHandler(UploadResponce);
            ClearForm();
            webclient.UploadStringAsync(uri, "POST", data);
            //Response.Write("<script type='text/javascript'> alert('Afspraak opgeslagen')</script>");             
            //}
            //catch (WebException wex)
            //{
            //    Response.Write("<script type='text/javascript'> alert('" + wex.Message + "')</script>");
            //}
            //catch (Exception ex)
            //{
            //    Response.Write("<script type='text/javascript'> alert('" + ex.Message + "')</script>");
            //}
        }

        private void UploadResponce(object sender, UploadStringCompletedEventArgs e)
        {
            if (e.Error == null && e.Cancelled != true)
                Response.Write("<script type='text/javascript'> alert('Afspraak opgeslagen')</script>");
            else
                Response.Write("<script type='text/javascript'> alert('" + e.Error.Message + "')</script>");
        }

        private void ClearForm()
        {
            string vandaag = DateTime.Today.ToShortDateString();
            startdatumTextBox.Text = vandaag;
            EinddatumTextBox.Text = vandaag;
            startUurDropDown.SelectedIndex = 8;
            eindUurDropDown.SelectedIndex = 17;
            KlantDropDown.SelectedIndex = 0;
            GroupDropDown.SelectedIndex = 0;
            TechniekerKeuzeDropDown.SelectedIndex = 0;
            OpvolgTextBox.Text = "";
            OnderwerpTextBox.Text = "";
            LocatieTextBox.Text = "";
            OmschrijvingTextBox.Text = "";
            //Wizard1.ActiveStepIndex = 0;
        }

        private Afspraak GetAfspraak()
        {
            List<string> geadresseerden = new List<string>();
            string strselected = selectedHiddenField.Value;
            string[] selected = strselected.Split(',');
            foreach (string tech in selected)
            {
                geadresseerden.Add(tech);
            }
            //string technieker = TechniekerDropDown.SelectedValue;
            string categorie = categorieDropDown.SelectedValue;
            string startdatum = Request.Form[startdatumTextBox.UniqueID];
            string einddatum = Request.Form[EinddatumTextBox.UniqueID];
            string startuur = startdatum + " " + Request.Form[startUurDropDown.UniqueID] + startMinuutDropDown.SelectedValue;
            string einduur = einddatum + " " + Request.Form[eindUurDropDown.UniqueID] + EindMinuutDropDown.SelectedValue;
            string locatie = Request.Form[LocatieTextBox.UniqueID];
            string omschrijving = Request.Form[OmschrijvingTextBox.UniqueID];
            string onderwerp = Request.Form[OnderwerpTextBox.UniqueID];
            string pas = passwordTextBox.Text;

            //geadresseerden.Add(technieker);
            Afspraak afspraak = new Afspraak();
            afspraak.Categorie = categorie;
            afspraak.Einduur = einduur;
            afspraak.Geadresseerden = geadresseerden;
            afspraak.Locatie = locatie;
            afspraak.Omschrijving = omschrijving;
            afspraak.Onderwerp = onderwerp;
            afspraak.Paswoord = pas;
            afspraak.Startuur = startuur;
            afspraak.UserEmail = "stefan.kelchtermans@itc.be";

            return afspraak;
        }

        protected void StartNextButton_Click(object sender, EventArgs e)
        {
            GetFilters();
        }

        private void GetFilters()
        {
            TicketFilter filter = new TicketFilter();
            filter.Klantid = KlantDropDown.SelectedItem.Value == "0" ? "" : KlantDropDown.SelectedItem.Value;
            filter.Groep = GroupDropDown.SelectedItem.Value;
            filter.Technieker = TechniekerKeuzeDropDown.SelectedItem.Value;//UserDropDown.SelectedItem.Text;

            DateTime? datum;
            if (!string.IsNullOrEmpty(OpvolgTextBox.Text))
                datum = DateConversions.GetDate(OpvolgTextBox.Text);
            else
                datum = null;
            filter.Opvolgdatum = datum;

            SharePointConnection con = new SharePointConnection();
            List<Ticket> tickets = con.GetTicketsByFilter(filter, hostweb, spapptoken, myrequest, drempelwaarde);
            TicketDropDown.Items.Clear();
            if (tickets != null)
            {
                foreach (Ticket ticket in tickets)
                {
                    ListItem item = new ListItem();
                    item.Value = ticket.ID.ToString();
                    item.Text = ticket.ID.ToString() + ": " + ticket.Omschrijving;
                    TicketDropDown.Items.Add(item);
                }
                string id = tickets[0].ID.ToString();
                GetInfo(id);
            }
        }

        protected void Wizard1_FinishButtonClick(object sender, WizardNavigationEventArgs e)
        {

        }

        protected void TicketDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList drop = (DropDownList)sender;
            string ticketid = drop.SelectedItem.Value;
            GetInfo(ticketid);
        }

        private void GetInfo(string ticketid)
        {
            SharePointConnection con = new SharePointConnection();
            Ticket ticket = con.GetTicketByID(ticketid, hostweb, spapptoken, myrequest);
            if (ticket != null)
            {
                LocatieTextBox.Text = ticket.KlantAdres;
                OmschrijvingTextBox.Text = "https://itcbe.sharepoint.com/Lists/Ticket/Dispform.aspx?ID=" + ticket.ID;
                OnderwerpTextBox.Text = ticket.Omschrijving;
            }
        }
    }
}